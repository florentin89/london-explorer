//
//  ViewController.swift
//  London_Explorer
//
//  Created by Florentin on 24/02/2019.
//  Copyright © 2019 Florentin Lupascu. All rights reserved.
//

import UIKit
import GoogleMaps

class MainViewController: UIViewController {
    
    // Properties
    var mapView: GMSMapView?
    var currentDestination: Destination?
    let destinations = [
                        Destination(name: "Buckingham Palace", subtitle: "London - SW1A 1AA", locationCoordinate: CLLocationCoordinate2D(latitude: 51.501571, longitude: -0.141042), zoom: 15),
                        Destination(name: "Big Ben", subtitle: "London - SW1A 0AA", locationCoordinate: CLLocationCoordinate2D(latitude: 51.500912, longitude: -0.124346), zoom: 15),
                        Destination(name: "London Eye", subtitle: "London - SE1 7PB", locationCoordinate: CLLocationCoordinate2D(latitude: 51.503514, longitude: -0.118645), zoom: 15),
                        Destination(name: "The Shard", subtitle: "London - SE1 9SG", locationCoordinate: CLLocationCoordinate2D(latitude: 51.504521, longitude: -0.086362), zoom: 15),
                        Destination(name: "Tower Bridge", subtitle: "London - SE1 2UP", locationCoordinate: CLLocationCoordinate2D(latitude: 51.504286, longitude: -0.076223), zoom: 15),
                        Destination(name: "Canary Wharf", subtitle: "London - E14 5NY", locationCoordinate: CLLocationCoordinate2D(latitude: 51.505369, longitude: -0.023897), zoom: 15)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create a GMSCameraPosition that tells the map to display the coordinate at zoom level 15 for The British Museum.
        let camera = GMSCameraPosition.camera(withLatitude: 51.519687, longitude: -0.126380, zoom: 15.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        // Create a button on the right corner
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next Destination", style: .plain, target: self, action: #selector(self.nextDestination))
        
        // Creates a marker in the center of the map with The British Museum
        let markerBritishMuseum = GMSMarker()
        markerBritishMuseum.position = CLLocationCoordinate2D(latitude: 51.519687, longitude: -0.126380)
        markerBritishMuseum.title = "The British Museum"
        markerBritishMuseum.snippet = "London - WC1B 3DG"
        markerBritishMuseum.map = mapView
        
        // Creates a marker in the center of the map with Buckingham Palace
        let markerBuckinghamPalace = GMSMarker()
        markerBuckinghamPalace.position = CLLocationCoordinate2D(latitude: 51.501571, longitude: -0.141042)
        markerBuckinghamPalace.title = "Buckingham Palace"
        markerBuckinghamPalace.snippet = "London - SW1A 1AA"
        markerBuckinghamPalace.map = mapView
        
        // Creates a marker in the center of the map with Big Ben
        let markerBigBen = GMSMarker()
        markerBigBen.position = CLLocationCoordinate2D(latitude: 51.500912, longitude: -0.124346)
        markerBigBen.title = "Big Ben"
        markerBigBen.snippet = "London - SW1A 0AA"
        markerBigBen.map = mapView
        
        // Creates a marker in the center of the map with London Eye
        let markerLondonEye = GMSMarker()
        markerLondonEye.position = CLLocationCoordinate2D(latitude: 51.503514, longitude: -0.118645)
        markerLondonEye.title = "London Eye"
        markerLondonEye.snippet = "London - SE1 7PB"
        markerLondonEye.map = mapView
        
        // Creates a marker in the center of the map with The Shard
        let markerTheShard = GMSMarker()
        markerTheShard.position = CLLocationCoordinate2D(latitude: 51.504521, longitude: -0.086362)
        markerTheShard.title = "The Shard"
        markerTheShard.snippet = "London - SE1 9SG"
        markerTheShard.map = mapView
        
        // Creates a marker in the center of the map with Tower Bridge
        let markerTowerBridge = GMSMarker()
        markerTowerBridge.position = CLLocationCoordinate2D(latitude: 51.504286, longitude: -0.076223)
        markerTowerBridge.title = "Tower Bridge"
        markerTowerBridge.snippet = "London - SE1 2UP"
        markerTowerBridge.map = mapView
        
        // Creates a marker in the center of the map with Canary Wharf
        let markerCanaryWharf = GMSMarker()
        markerCanaryWharf.position = CLLocationCoordinate2D(latitude: 51.505369, longitude: -0.023897)
        markerCanaryWharf.title = "Canary Wharf"
        markerCanaryWharf.snippet = "London - E14 5NY"
        markerCanaryWharf.map = mapView
        
    }
    
    @objc func nextDestination(){
        
        if currentDestination == nil || destinations.lastIndex(of: currentDestination!) == destinations.count-1 {
            currentDestination = destinations.first
        } else {
            if let index = destinations.firstIndex(of: currentDestination!) {
                if index < destinations.count - 1 {
                currentDestination = destinations[index + 1]
                }
            }
        }
        
        setMapCameraSpeed()
    }
    
    func setMapCameraSpeed() {
        CATransaction.begin()
        CATransaction.setValue(2, forKey: kCATransactionAnimationDuration)
        mapView?.animate(to: GMSCameraPosition.camera(withTarget: currentDestination!.locationCoordinate, zoom: currentDestination!.zoom))
        CATransaction.commit()
        
        let marker = GMSMarker(position: currentDestination!.locationCoordinate)
        marker.title = currentDestination?.name
        marker.map = mapView
    }
    
    
}


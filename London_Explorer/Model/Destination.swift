//
//  Destination.swift
//  London_Explorer
//
//  Created by Florentin on 24/02/2019.
//  Copyright © 2019 Florentin Lupascu. All rights reserved.
//

import UIKit
import GoogleMaps

class Destination: NSObject {
    
    let name: String
    let subtitle: String
    let locationCoordinate: CLLocationCoordinate2D
    let zoom: Float
    
    init(name: String, subtitle: String, locationCoordinate: CLLocationCoordinate2D, zoom: Float) {
        self.name = name
        self.subtitle = subtitle
        self.locationCoordinate = locationCoordinate
        self.zoom = zoom
    }
}

